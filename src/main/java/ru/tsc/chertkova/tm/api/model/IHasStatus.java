package ru.tsc.chertkova.tm.api.model;

import ru.tsc.chertkova.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
